package bdtc.lab1;

import lombok.extern.log4j.Log4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


@Log4j
public class MapReduceApplication {

    public static void main(String[] args) throws Exception {

        IntWritable scale = new IntWritable(1);

        if (args.length < 3) {
            throw new RuntimeException("USAGE: yarn jar myLR1/target/myLR1-with-dependencies.jar " +
                    "<input folder> <output folder> <id-namee catalog> [<scale>]");
        }

        if (args.length == 4) {
            scale = new IntWritable(Integer.parseInt(args[3]));
        }

        Configuration conf = new Configuration();

        conf.set("fs.default.name","hdfs://127.0.0.1:9000"); // set our file system to find catalog, that user chose
        conf.set("mapreduce.output.textoutputformat.separator", ","); // csv output
        conf.set("scale", scale.toString()); // set scale to job configuration
        conf.set("catalog", args[2]); // set id-name catalog to job configuration

        Job job = Job.getInstance(conf, "browser count");
        job.setJarByClass(MapReduceApplication.class);
        job.setMapperClass(LR1Mapper.class);
        job.setReducerClass(LR1Reducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(OutputValue.class);  // set output value for map using custom type
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        Path outputDirectory = new Path(args[1]);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, outputDirectory);
        log.info("=====================JOB STARTED=====================");
        job.waitForCompletion(true);
        log.info("=====================JOB ENDED=====================");
    }
}
