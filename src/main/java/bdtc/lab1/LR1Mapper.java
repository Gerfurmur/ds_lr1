package bdtc.lab1;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Mapper;

//import javax.security.auth.login.Configuration;
import java.io.IOException;

/** parse input data
 *  input data = id,timestamp,value
 *  border timestamp by period that is set by scale
 */

public class LR1Mapper extends Mapper<LongWritable, Text, Text, OutputValue> {

    long scale;
    OutputValue outValue;

    @Override
    protected void setup(Context context){
        Configuration conf = context.getConfiguration();
        scale = Long.parseLong(conf.get("scale", "1")); // get scale from job conf
        outValue = new OutputValue(); // create custom type var to not define it every time
    }

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        try {
            String[] inputData = value.toString().split(","); // input line = id,timestamp,value
            long tmpTimestamp = Long.parseLong(inputData[1]); // get timestamp from input data

            // set outValue and make key = id,timestamp where timestamp is bordered by period (scale)
            outValue.setValue(new IntWritable(Integer.parseInt(inputData[2])));

            context.write(new Text(inputData[0] + "," +
                    (tmpTimestamp - tmpTimestamp % (scale * 60L * 1000L))), outValue);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
