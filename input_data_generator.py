import mimesis
import datetime
import random
import os
import subprocess
#import argsparser
import sys


THIS_PATH = os.path.abspath(os.path.dirname(__file__))


def generate(output_file, catalog, amount=1000, value_start=1, value_end=100, timerange=10):
    gen1 = mimesis.Hardware()
    gen2 = mimesis.Address()
    funcs = [gen1.graphics, gen1.cpu, gen2.city]
    
    now = int(datetime.datetime.now().timestamp() * 1000)
    index = 1
    
    catalog_d = {}
    
    with open(output_file, 'w') as wf:
        for i in range(1, amount + 1):
            el = random.choice(funcs)()
            catalog_d.setdefault(el, str(index))
            
            index += 1
            for j in range(1000):
                data = [catalog_d[el], str(now + random.randint(0, 60 * 1000 * timerange)), str(random.randint(value_start, value_end))]
                
                wf.write(','.join(data) + '\n')
                
    
    with open(catalog, 'w') as wf:
        wf.write('\n'.join("%s,%s" % (catalog_d[key], key) for key in catalog_d))


def claen_hdfs(output_file, catalog, job_out_path):
    cmd = "hdfs dfs -rm -r /%s /%s /%s" % (os.path.basename(output_file), os.path.basename(catalog), job_out_path)
    subprocess.call(cmd, shell=True)


def copy_to_hdfs(output_file, catalog, job_out_path):
    cmd = "hdfs dfs -put %s /%s" % (output_file, os.path.basename(output_file))
    subprocess.call(cmd, shell=True)
    cmd = "hdfs dfs -put %s /%s" % (catalog, os.path.basename(catalog))
    subprocess.call(cmd, shell=True)
    

def print_start_cmd(output_file, catalog, job_out_path, scale=1):
    print("START JOB COMMAND")
    print("yarn jar %s/target/lab1-1.0-SNAPSHOT-jar-with-dependencies.jar /%s /%s /%s %s" % (THIS_PATH, os.path.basename(output_file), job_out_path, os.path.basename(catalog), scale))


def arg_parse():
    print("USAGE: python3 input_data_generator.py <input data filename> <output path> <scale> <amount of generating data outer loop> <value start> <value end> <timerange>")
    # FIXME
    try:
        _fname_pat = sys.argv[1]
    except IndexError:
        _fname_pat = "input_data"
    try:
        _out = sys.argv[2]
    except IndexError:
        _out = "out"
    try:
        _scale = sys.argv[3]
    except IndexError:
        _scale = "1"
    try:
        _amount = sys.argv[4]
    except IndexError:
        _amount = "100"
    try:
        _v_start = sys.argv[5]
    except IndexError:
        _v_start = "1"
    try:
        _v_end = sys.argv[6]
    except IndexError:
        _v_end = "100"
    try:
        _timerange = sys.argv[7]
    except IndexError:
        _timerange = "10"
    return _fname_pat, _out, _scale, _amount, _v_start, _v_end, _timerange



if __name__ == "__main__":
    _fname_pat, _out, _scale, _amount, _v_start, _v_end, _timerange = arg_parse()
    
    os.makedirs(os.path.join(THIS_PATH, "tmp"), exist_ok=True)
    
    output_file = os.path.abspath(os.path.join(THIS_PATH, "tmp", _fname_pat + ".in"))
    catalog = os.path.abspath(os.path.join(THIS_PATH, "tmp", _fname_pat + ".catalog"))
    job_out_path = _out
    
    generate(output_file, catalog, int(_amount), int(_v_start), int(_v_end), int(_timerange) - 1)
    claen_hdfs(output_file, catalog, job_out_path)
    copy_to_hdfs(output_file, catalog, job_out_path)
    print_start_cmd(output_file, catalog, job_out_path, _scale)
    


