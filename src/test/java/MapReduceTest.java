import bdtc.lab1.LR1Mapper;
import bdtc.lab1.LR1Reducer;
import bdtc.lab1.OutputValue;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MapReduceTest {

    private MapDriver<LongWritable, Text, Text, OutputValue> mapDriver;
    private ReduceDriver<Text, OutputValue, Text, Text> reduceDriver;
    private MapReduceDriver<LongWritable, Text, Text, OutputValue, Text, Text> mapReduceDriver;

    private final String testIn = "1,1510670955159,15"; // id = 1, timestamp = 1510670955159, value = 15

    @Before
    public void setUp() {
        // default scale for job = 1, catalog does not exist, so id 1 = NodeCPUIntel by default
        LR1Mapper mapper = new LR1Mapper();
        LR1Reducer reducer = new LR1Reducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(new LongWritable(), new Text(testIn))
                .withOutput(new Text("1,1510670940000"), new OutputValue(new IntWritable(15)))
                .runTest(); // out key - id, timestamp, where timestamp bordered by 1m period
    }

    @Test
    public void testReducer() throws IOException {
        List<OutputValue> values = new ArrayList<OutputValue>();
        values.add(new OutputValue(new IntWritable(15)));
        values.add(new OutputValue(new IntWritable(15)));
        reduceDriver
                .withInput(new Text("1,1510670940000"), values)
                .withOutput(new Text("NodeCPUIntel,1510670940000"), new Text("1m,15.0"))
                .runTest();  // default id 1 = NodeCPUIntel
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(new LongWritable(), new Text(testIn))
                .withInput(new LongWritable(), new Text(testIn))
                .withOutput(new Text("NodeCPUIntel,1510670940000"), new Text("1m,15.0"))
                .runTest();
    }
}

