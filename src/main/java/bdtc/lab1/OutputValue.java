package bdtc.lab1;

import java.io.DataInput;
import java.io.DataOutput;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;

import java.io.IOException;

public class OutputValue implements Writable {
    private IntWritable value; // in value

    //default constructor
    public OutputValue() {
        value = new IntWritable(0);
    }

    public OutputValue(IntWritable value) {
        this.value = value;
    }

    public void write(DataOutput dataOutput) throws IOException {
        value.write(dataOutput); //write value
    }

    public void readFields(DataInput dataInput) throws IOException {
        value.readFields(dataInput); //read value
    }

    public IntWritable getValue() {
        return value;
    }

    public void setValue(IntWritable value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    public boolean equals(Object o) {
        return this.toString().equals(o.toString()); // just compare string of objects
    }
}