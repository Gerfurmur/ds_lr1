import bdtc.lab1.OutputValue;
import bdtc.lab1.LR1Mapper;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;


public class OutputValueTest {

    private OutputValue out = new OutputValue();

    @Test
    public void testStandardConstructor() throws IOException  {
        assertEquals(0, out.getValue().get()); // standard - value = 0
    }
    @Test
    public void testConstructor() throws IOException  {
        OutputValue out1 = new OutputValue(new IntWritable(10)); // create outputvalue with value=10
        assertEquals(10, out1.getValue().get());
    }
    @Test
    public void testSet() throws IOException  {
        out.setValue(new IntWritable(1));
        assertEquals(1, out.getValue().get()); // at start value = 0, after set value = 1
    }
    @Test
    public void testToString() throws IOException  {
        assertEquals("0", out.toString());
    }
    @Test
    public void testEquals() throws IOException  {
        assertEquals(true, out.equals("0"));
    }
}

