package bdtc.lab1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * reducer: get average value for id in the period
 */
public class LR1Reducer extends Reducer<Text, OutputValue, Text, Text> {

    String catalog;
    String scale;
    Map<String,String> dictionary = new HashMap<String,String>();

    @Override
    protected void setup(Context context) throws java.io.FileNotFoundException {
        Configuration conf = context.getConfiguration();
        scale = conf.get("scale", "1");
        catalog = conf.get("catalog","None"); // get id-name catalog from job conf

        try {
            FileSystem fileSystem = FileSystem.get(conf);
            // open catalog that stored in hdfs, read data, split by \n and iterate
            Path path = new Path(catalog);
            FSDataInputStream in = fileSystem.open(path);

            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line = null;
            while((line = br.readLine())!= null){
                // parse data in catalog, where each line - id,name
                try {
                    dictionary.put(line.split(",")[0], line.split(",")[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            dictionary.put("1", "NodeCPUIntel"); // default value for tests
        }
    }

    @Override
    protected void reduce(Text key, Iterable<OutputValue> values, Context context) throws IOException, InterruptedException {
        int sum = 0, count = 0;

        // get average value for id in a period that equal <scale> minutes
        while (values.iterator().hasNext()) {
            sum += values.iterator().next().getValue().get();
            count++;
        }

        context.write(new Text(dictionary.get(key.toString().split(",")[0]) + "," +
                key.toString().split(",")[1]), new Text(scale + "m," + ((float)sum / count)));
    }
}
